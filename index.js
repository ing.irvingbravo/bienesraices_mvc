import express from 'express';
import csrf from 'csurf'
import cookieParser from 'cookie-parser';
import UserRoutes from './routes/UserRoutes.js'
import db from './config/db.js'

// Crear app
const app = express()

//Habilitar lectura de datos
app.use(express.urlencoded({ extended: true }))

//Habilitar Cookie Parser
app.use(cookieParser())

//Habilitar CSRF
app.use(csrf({ cookie: true }))

//Conexión a la base de datos
try {
    await db.authenticate();
    db.sync()
    console.log('Conexión correcta a la BD')
} catch (error) {
    console.log(error)
}

//Habilitar Pug
app.set('view engine', 'pug')
app.set('views', './views')

//Carpeta publica
app.use(express.static('public'))
//Routing
app.use('/auth', UserRoutes)
// Definir puerto y arrancar proyecto
const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log("El servidor esta funcionando en el puerto", port)
})