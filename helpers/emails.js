import nodemailer from 'nodemailer'

const emailRegister = async (data) => {
    const transport = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD
        }
    });

    const {name, email, token} = data

    //enviar email
    await transport.sendMail({
        from: 'BienesRaices.com',
        to: email,
        subject: 'Confirm your account',
        text: 'Confirm your account',
        html: `
        <p>Hello ${name}, confirm your account in bienesraicer.com</p>
        
        <p>Tu cuenta ya esta lista, solo confirmala en el siguiente enlace:
        <a href="${process.env.BACKEND_URL}:${process.env.PORT ?? 3000}/auth/confirm/${token}">Confirmar Cuenta </a></p>

        <p>Si tu no creaste esta cuenta, puedes ignorar el mensaje</p>
        `
        
    })
}

const emailPassword = async (data) => {
    const transport = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD
        }
    });

    const {name, email, token} = data

    //enviar email
    await transport.sendMail({
        from: 'BienesRaices.com',
        to: email,
        subject: 'Restablecer contraseña',
        text: 'Restablecer contraseña',
        html: `
        <p>Hola ${name}, has solicitado restablecer tu contraseña en bienesraicer.com</p>
        
        <p>Ingresa al siguiente enlace para generar una contraseña nueva:
        <a href="${process.env.BACKEND_URL}:${process.env.PORT ?? 3000}/auth/forgot-password/${token}">Restablece tu contraseña</a></p>

        <p>Si tu no solicitaste el cambio de contraseña, puedes ignorar el mensaje</p>
        `
        
    })
}

export {
    emailRegister,
    emailPassword
}