import {check, validationResult} from 'express-validator'
// const {check, validationResult} = require('express-validator');

export const UserCreate =  [
    check('name').notEmpty().withMessage('No puede ir vacio').run(req),
    check('email').isEmail().withMessage('Agrega un email correcto').run(req),
    check('password').isLength({ min: 8}).withMessage('El password debe contener al menos 8 caracteres').run(req),
    check('confirm_password').equals('password').withMessage('No puede ir vacio').run(req),    
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
          return res.status(422).json({errors: errors.array()});
        next();
      },
];