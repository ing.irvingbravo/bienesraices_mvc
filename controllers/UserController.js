import { check, validationResult } from 'express-validator'
import User from '../models/User.js'
import { generarId } from '../helpers/tokens.js'
import { emailRegister, emailPassword } from '../helpers/emails.js'
import bcrypt from 'bcrypt'

const formLogin = (req, res) => {
    res.render('auth/login', {
        page: 'Login',
        csrfToken: req.csrfToken()
    })
}

const authenticate = async (req, res) => {
    // Validacion
    await check('email').isEmail().withMessage('The email is mandatory').run(req)
    await check('password').notEmpty().withMessage('The password is mandatory').run(req)

    let result = validationResult(req)

    if (!result.isEmpty()) {
        return res.render('auth/login', {
            page: 'Login',
            errors: result.array(),
            csrfToken: req.csrfToken()
        })
    }

    const { email, password } = req.body

    // Comprobar si el usuario existe
    const user = await User.findOne({where: {email}})
    if (!user) {
        return res.render('auth/login', {
            page: 'Login',
            errors: [{msg: 'El usuario no existe'}],
            csrfToken: req.csrfToken()
        })
    }

    // Comprobar si el usuario esta confirmado
    if (!user.confirm) {
        return res.render('auth/login', {
            page: 'Login',
            errors: [{msg: 'El usuario no ha confirmado'}],
            csrfToken: req.csrfToken()
        })
    }

    // Revisar el password
    if (!user.verifyPassword(password)) {
        return res.render('auth/login', {
            page: 'Login',
            errors: [{msg: 'EL password es incorrecto'}],
            csrfToken: req.csrfToken()
        })
    }

    // Autenticar el usuario
}

const formRegister = (req, res) => {
    res.render('auth/register', {
        page: 'Create account',
        csrfToken: req.csrfToken()
    })
}
const register = async (req, res) => {
    //Validación
    await check('name').notEmpty().withMessage('The name cannot be empty').run(req)
    await check('email').isEmail().withMessage('Add a correct email').run(req)
    await check('password').isLength({ min: 8 }).withMessage('The password must contain at least 8 characters').run(req)
    await check('confirmPassword').equals(req.body.password).withMessage('Passwords do not match').run(req)

    let result = validationResult(req)

    if (!result.isEmpty()) {
        console.log(req.body)
        return res.render('auth/register', {
            page: 'Create account',
            errors: result.array(),
            user: {
                name: req.body.name,
                email: req.body.email
            },
            csrfToken: req.csrfToken()
        })
    }

    //Extraer los datos
    const { name, email, password } = req.body

    //Verificar que el email no este dublicado
    const userExist = await User.findOne({ where: { email } })
    if (userExist) {
        return res.render('auth/register', {
            page: 'Create account',
            errors: [{ msg: 'There is already an account with that email' }],
            user: {
                name: req.body.name,
                email: req.body.email
            },
            csrfToken: req.csrfToken()
        })
    }

    //Crear cuenta
    const user = await User.create({
        name,
        email,
        password,
        token: generarId()
    })

    //enviar correo de confirmacion
    emailRegister({
        name: user.name,
        email: user.email,
        token: user.token
    })

    res.render('templates/message', {
        page: 'account created successfully',
        msg: 'We have sent a confirmation email, click on the link'
    })
}

const formForgotPassword = (req, res) => {
    res.render('auth/forgotPassword', {
        page: 'Forgot password',
        csrfToken: req.csrfToken()
    })
}

const resetPassword = async (req, res) => {
    await check('email').isEmail().withMessage('Add a correct email').run(req)

    let result = validationResult(req)

    if (!result.isEmpty()) {
        return res.render('auth/forgotPassword', {
            page: 'Forgot password',
            csrfToken: req.csrfToken(),
            errors: result.array(),
        })
    }

    //Buscar el usuario
    const { email } = req.body
    const user = await User.findOne({ where: { email } })

    if (!user) {
        return res.render('auth/forgotPassword', {
            page: 'Forgot password',
            errors: [{ msg: 'el email no pertenece a ningun usuario' }],
        })
    }

    //Generar token y enviar email
    user.token = generarId();
    await user.save();

    emailPassword({
        email: user.email,
        name: user.name,
        token: user.token
    })

    res.render('templates/message', {
        page: 'Reestablece tu contraseña',
        msg: 'Hemos enviado un email con las instrucciones'
    })

}

const comprobarToken = async (req, res) => {
    const { token } = req.params;
    //verificar token
    const user = await User.findOne({ where: { token } })

    if (!user) {
        return res.render('auth/confirmPassword', {
            page: 'reestablece tu contraseña',
            msg: 'Hubo un error al validar tu información, intenta de nuevo',
            error: true
        })
    }

    //mostrar form
    res.render('auth/resetPassword', {
        page: 'Reestablece tu password',
        csrfToken: req.csrfToken()
    })

}

const nuevoPassword = async (req, res) => {
    // Valdiar password
    await check('password').isLength({ min: 8 }).withMessage('The password must contain at least 8 characters').run(req)

    let result = validationResult(req)

    // Verificar que el resultado este vacio
    if (!result.isEmpty()) {
        // Erorres
        return res.render('auth/resetPassword', {
            page: 'Reestablece tu password',
            errors: result.array(),
            user: {
                name: req.body.name,
                email: req.body.email
            },
            csrfToken: req.csrfToken()
        })
    }

    const { token } = req.params
    const { password } = req.body;

    // Identificar quien hace el password
    const user = await User.findOne({ where: { token } })

    // Hashear el nuevo password
    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(password, salt);
    user.token = null;

    // Guardar password
    await user.save();

    res.render('auth/confirmPassword', {
        page: 'Password Resstablecido',
        msg: 'El password se guardo correctamente'
    })

}

const confirm = async (req, res) => {
    const { token } = req.params;
    //verificar token
    const user = await User.findOne({ where: { token } })

    if (!user) {
        return res.render('auth/confirmPassword', {
            page: 'error al confirmar tu cuenta',
            msg: 'Hubo un error al confirmar tu cuenta intenta de nuevo',
            error: true
        })
    }

    //confirmar usuario
    user.token = null;
    user.confirm = true;
    await user.save();

    return res.render('auth/confirmPassword', {
        page: 'Cuenta confirmada',
        msg: 'Lacuenta se confirmó correctamente'
    })

}

export {
    formLogin,
    authenticate,
    formRegister,
    formForgotPassword,
    register,
    confirm,
    resetPassword,
    comprobarToken,
    nuevoPassword
}