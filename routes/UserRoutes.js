import express from "express";
import { formLogin, formRegister, formForgotPassword, register, confirm, resetPassword,
comprobarToken, nuevoPassword, authenticate } from "../controllers/UserController.js";

const router = express.Router();

// Routing
router.get('/login', formLogin);
router.post('/login', authenticate);
router.get('/register',  formRegister)
router.post('/register',  register)
router.get('/forgot-password',  formForgotPassword)
router.post('/forgot-password',  resetPassword)
router.get('/confirm/:token', confirm)
router.get('/forgot-password/:token', comprobarToken)
router.post('/forgot-password/:token', nuevoPassword)

export default router



